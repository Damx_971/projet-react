import Alcool from "../models/Alcool";

export default class AlcoolController{

    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            let alcools = await Alcool.find().select('-__v ');
            body = {alcools};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Alcool list',
                message: e.message || 'An error is occured into alcool list',
            }
        }
        return res.status(status).json(body);
    }

    static async add(req, res){
        let status = 200;
        let body = {};

        try{
            let alcool = await Alcool.create(req.body);
            body = {alcool};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Alcool create',
                message: e.message || 'An error is occured into alcool create',
            }
        }
        return res.status(status).json(body);
    }

    static async update(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let alcool = await Alcool.findByIdAndUpdate(id, req.body, {new: true})
                .select('-__v');
            body = {alcool};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Alcool update',
                message: e.message || 'An error is occured into alcool update',
            }
        }
        return res.status(status).json(body);
    }

    static async remove(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            await Alcool.findByIdAndDelete(id);
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Alcool delete',
                message: e.message || 'An error is occured into alcool delete',
            }
        }
        return res.status(status).json(body);
    }

}

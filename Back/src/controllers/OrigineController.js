import Origine from "../models/Origine";

export default class OrigineController{

    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            let origines = await Origine.find().select('-__v ');
            body = {origines};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Origine list',
                message: e.message || 'An error is occured origine user list',
            }
        }
        return res.status(status).json(body);
    }

    static async add(req, res){
        let status = 200;
        let body = {};

        try{
            let origine = await Origine.create(req.body);
            body = {origine};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Origine create',
                message: e.message || 'An error is occured into origine create',
            }
        }
        return res.status(status).json(body);
    }

    static async update(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let origine = await Origine.findByIdAndUpdate(id, req.body, {new: true})
                .select('-__v');
            body = {origine};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Origine update',
                message: e.message || 'An error is occured into origine update',
            }
        }
        return res.status(status).json(body);
    }

    static async remove(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            await Origine.findByIdAndDelete(id);
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Origine delete',
                message: e.message || 'An error is occured into origine delete',
            }
        }
        return res.status(status).json(body);
    }

}

import Residence from "../models/Residence";

export default class ResidenceController{

    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            let residences = await Residence.find().select('-__v');
            body = {residences};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Residence list',
                message: e.message || 'An error is occured into residence list',
            }
        }
        return res.status(status).json(body);
    }

    static async add(req, res){
        let status = 200;
        let body = {};

        try{
            let residence = await Residence.create(req.body);
            body = {residence};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Residence create',
                message: e.message || 'An error is occured into residence create',
            }
        }
        return res.status(status).json(body);
    }

    static async update(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let residence = await Residence.findByIdAndUpdate(id, req.body, {new: true})
                .select('-__v');
            body = {residence};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Residence update',
                message: e.message || 'An error is occured into residence update',
            }
        }
        return res.status(status).json(body);
    }

    static async remove(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            await Residence.findByIdAndDelete(id);
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Residence delete',
                message: e.message || 'An error is occured into residence delete',
            }
        }
        return res.status(status).json(body);
    }

}

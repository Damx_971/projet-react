import User from "../models/User";
import jsonwebtoken from 'jsonwebtoken';
import fs from 'fs';


export default class UserController{

    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            let users = await User.find().select('-__v -password');
            body = {users};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User list',
                message: e.message || 'An error is occured into user list',
            }
        }
        return res.status(status).json(body);
    }

    static async details(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let user = await User.findById(id).select('-__v -password');
            body = {user};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User details',
                message: e.message || 'An error is occured into user details',
            }
        }
        return res.status(status).json(body);
    }

    static async auth(req, res){
        let status = 200;
        let body = {};

        try{
            let {email, password} = req.body;
            let user = await User.findOne({'email': email}).select('-__v');
            if(user && password === user.password){
                let {JWT_SECRET} = process.env;

                let token = jsonwebtoken.sign({sub: user._id}, JWT_SECRET);
                body = {user, token};
            }else{
                status = 401;
                new Error('Unauthorized');
            }
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User authentication',
                message: e.message || 'An error is occured into user auth',
            }
        }
        return res.status(status).json(body);
    }

    static async add(req, res){
        let status = 200;
        let body = {};

        try{
            let user = await User.create(req.body);
            body = {user};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User create',
                message: e.message || 'An error is occured into user create',
            }
        }
        return res.status(status).json(body);
    }

    static async update(req, res){
        let status = 200;
        let body = {};

        try{
            delete req.body.thumbnail;
            let {id} = req.params;
            let user = await User.findByIdAndUpdate(id, req.body, {new: true})
                .populate('ville').populate('thumbnail') .populate('apparence').populate('origine').populate('residence')
                .populate('sexualite').populate('situation').populate('alcool').populate('tabac');
            body = {user};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User update',
                message: e.message || 'An error is occured into user update',
            }
        }
        return res.status(status).json(body);
    }

    static async updateThumbnail(req, res){
        let status = 200;
        let body = {};

        try{
            let file = await User.findById(req.params.id);
            if(fs.existsSync(`./${file.thumbnail}`)){
                await fs.unlinkSync(`./${file.thumbnail}`);
            }
            file.thumbnail = req.body.thumbnail;
            await file.save();
            body = {file};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Update thumbnail',
                message: e.message || 'An error is occured into thumbnail updated',
            }
        }
        return res.status(status).json(body);
    }

    static async remove(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let user = await User.findByIdAndDelete(id);
            if(fs.existsSync(`./${user.thumbnail}`)){
                await fs.unlinkSync(`./${user.thumbnail}`);
            }
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User delete',
                message: e.message || 'An error is occured into user delete',
            }
        }
        return res.status(status).json(body);
    }

}

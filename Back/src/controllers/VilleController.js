import Ville from "../models/Ville";

export default class VilleController{

    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            let villes = await Ville.find().select('-__v ');
            body = {villes};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Ville list',
                message: e.message || 'An error is occured into ville list',
            }
        }
        return res.status(status).json(body);
    }

}

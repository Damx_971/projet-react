import Apparence from "../models/Apparence";

export default class ApparenceController{

    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            let apparences = await Apparence.find().select('-__v ');
            body = {apparences};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Apparence list',
                message: e.message || 'An error is occured into apparence list',
            }
        }
        return res.status(status).json(body);
    }

    static async add(req, res){
        let status = 200;
        let body = {};

        try{
            let apparence = await Apparence.create(req.body);
            body = {apparence};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Apparence create',
                message: e.message || 'An error is occured into apparence create',
            }
        }
        return res.status(status).json(body);
    }

    static async update(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let apparence = await Apparence.findByIdAndUpdate(id, req.body, {new: true})
                .select('-__v');
            body = {apparence};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Apparence update',
                message: e.message || 'An error is occured into apparence update',
            }
        }
        return res.status(status).json(body);
    }

    static async remove(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            await Apparence.findByIdAndDelete(id);
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Apparence delete',
                message: e.message || 'An error is occured into apparence delete',
            }
        }
        return res.status(status).json(body);
    }

}

import Situation from "../models/Situation";

export default class SituationController{

    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            let situations = await Situation.find().select('-__v ');
            body = {situations};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Situation list',
                message: e.message || 'An error is occured into situation list',
            }
        }
        return res.status(status).json(body);
    }

    static async add(req, res){
        let status = 200;
        let body = {};

        try{
            let situation = await Situation.create(req.body);
            body = {situation};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Situation create',
                message: e.message || 'An error is occured into situation create',
            }
        }
        return res.status(status).json(body);
    }

    static async update(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let situation = await Situation.findByIdAndUpdate(id, req.body, {new: true})
                .select('-__v');
            body = {situation};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Situation update',
                message: e.message || 'An error is occured into situation update',
            }
        }
        return res.status(status).json(body);
    }

    static async remove(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            await Situation.findByIdAndDelete(id);
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Situation delete',
                message: e.message || 'An error is occured into situation delete',
            }
        }
        return res.status(status).json(body);
    }

}

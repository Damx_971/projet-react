import Tabac from "../models/Tabac";

export default class TabacController{

    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            let tabacs = await Tabac.find().select('-__v');
            body = {tabacs};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Tabac list',
                message: e.message || 'An error is occured into tabac list',
            }
        }
        return res.status(status).json(body);
    }

    static async add(req, res){
        let status = 200;
        let body = {};

        try{
            let tabac = await Tabac.create(req.body);
            body = {tabac};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Tabac create',
                message: e.message || 'An error is occured into tabac create',
            }
        }
        return res.status(status).json(body);
    }

    static async update(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let tabac = await Tabac.findByIdAndUpdate(id, req.body, {new: true})
                .select('-__v');
            body = {tabac};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Tabac update',
                message: e.message || 'An error is occured into tabac update',
            }
        }
        return res.status(status).json(body);
    }

    static async remove(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            await Tabac.findByIdAndDelete(id);
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Tabac delete',
                message: e.message || 'An error is occured into tabac delete',
            }
        }
        return res.status(status).json(body);
    }

}

import Sexualite from "../models/Sexualite";

export default class SexualiteController{

    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            let sexualites = await Sexualite.find().select('-__v ');
            body = {sexualites};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Sexualite list',
                message: e.message || 'An error is occured into sexualite list',
            }
        }
        return res.status(status).json(body);
    }

    static async add(req, res){
        let status = 200;
        let body = {};

        try{
            let sexualite = await Sexualite.create(req.body);
            body = {sexualite};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Sexualite create',
                message: e.message || 'An error is occured into sexualite create',
            }
        }
        return res.status(status).json(body);
    }

    static async update(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let sexualite = await Sexualite.findByIdAndUpdate(id, req.body, {new: true})
                .select('-__v');
            body = {sexualite};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Sexualite update',
                message: e.message || 'An error is occured into sexualite update',
            }
        }
        return res.status(status).json(body);
    }

    static async remove(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            await Sexualite.findByIdAndDelete(id);
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Sexualite delete',
                message: e.message || 'An error is occured into sexualite delete',
            }
        }
        return res.status(status).json(body);
    }

}

import { Schema, model } from 'mongoose';

const AlcoolSchema = new Schema({
    libelle: {type: String, required: true},
});

export default model('Alcool', AlcoolSchema);

import { Schema, model } from 'mongoose';

const UserSchema = new Schema({
    nom: {type: String, required: true},
    prenom: {type: String, required: true},
    email: {type: String, unique: true, required: true},
    password: {type: String, required: true},
    sexe: {type: String, required: true},
    datenaiss: {type: Date, required: true},
    ville: {type: Schema.Types.ObjectId, ref: 'Ville', required: true},
    thumbnail: {type: String, required: true},
    aboutme: {type: String},
    apparence: {type: Schema.Types.ObjectId, ref: 'Apparence', required: true},
    taille: {type: Number, required: true},
    origine: {type: Schema.Types.ObjectId, ref: 'Origine', required: true},
    enfants: {type: Number},
    residence: {type: Schema.Types.ObjectId, ref: 'Residence', required: true},
    sexualite: {type: Schema.Types.ObjectId, ref: 'Sexualite', required: true},
    situation: {type: Schema.Types.ObjectId, ref: 'Situation', required: true},
    alcool: {type: Schema.Types.ObjectId, ref: 'Alcool', required: true},
    tabac: {type: Schema.Types.ObjectId, ref: 'Tabac', required: true},
    role: {type: Number, default: 0},
    created_at: {type: Date, default: Date.now()},
});

export default model('User', UserSchema);

import { Schema, model } from 'mongoose';

const ApparenceSchema = new Schema({
    libelle: {type: String, required: true},
});

export default model('Apparence', ApparenceSchema);

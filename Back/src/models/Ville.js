import { Schema, model } from 'mongoose';

const VilleSchema = new Schema({
    nom: {type: String, required: true},
    code: {type: Number, required: true},
});

export default model('Ville', VilleSchema);

import { Schema, model } from 'mongoose';

const SituationSchema = new Schema({
    libelle: {type: String, required: true},
});

export default model('Situation', SituationSchema);

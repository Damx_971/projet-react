import { Schema, model } from 'mongoose';

const SexualiteSchema = new Schema({
    libelle: {type: String, required: true},
});

export default model('Sexualite', SexualiteSchema);

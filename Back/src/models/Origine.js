import { Schema, model } from 'mongoose';

const OrigineSchema = new Schema({
    libelle: {type: String, required: true},
});

export default model('Origine', OrigineSchema);

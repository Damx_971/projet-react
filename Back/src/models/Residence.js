import { Schema, model } from 'mongoose';

const ResidenceSchema = new Schema({
    libelle: {type: String, required: true},
});

export default model('Residence', ResidenceSchema);

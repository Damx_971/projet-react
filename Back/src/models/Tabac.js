import { Schema, model } from 'mongoose';

const TabacSchema = new Schema({
    libelle: {type: String, required: true},
});

export default model('Tabac', TabacSchema);

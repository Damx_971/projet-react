import { Router } from 'express';
import UserController from "./controllers/UserController";
import ApparenceController from "./controllers/ApparenceController";
import OrigineController from "./controllers/OrigineController";
import ResidenceController from "./controllers/ResidenceController";
import SituationController from "./controllers/SituationController";
import SexualiteController from "./controllers/SexualiteController";
import AlcoolController from "./controllers/AlcoolController";
import TabacController from "./controllers/TabacController";
import VilleController from "./controllers/VilleController";
import Auth from "./utils/Auth";
import Multer from "./utils/Multer";

const router = Router();

/**
 * Users
 */
router.get('/users', Auth.isAllowed([10]), UserController.list);//role 10 = admin
router.get('/users/:id', UserController.details);
router.post('/users/auth', UserController.auth);//connexion
router.post('/users', Multer.upload('photos', 'thumbnail'), UserController.add);//inscription
router.put('/users/:id', UserController.update);
router.put('/users/:id/thumbnail', Multer.upload('photos', 'thumbnail'), UserController.updateThumbnail);
router.delete('/users/:id', UserController.remove);


/**
 * Apparences
 */
router.get('/apparences', ApparenceController.list);
router.post('/apparences', Auth.isAllowed([10]), ApparenceController.add);
router.put('/apparences/:id', Auth.isAllowed([10]), ApparenceController.update);
router.delete('/apparences/:id', Auth.isAllowed([10]), ApparenceController.remove);


/**
 * Origines
 */
router.get('/origines', OrigineController.list);
router.post('/origines', Auth.isAllowed([10]), OrigineController.add);
router.put('/origines/:id', Auth.isAllowed([10]), OrigineController.update);
router.delete('/origines/:id', Auth.isAllowed([10]), OrigineController.remove);


/**
 * Residences
 */
router.get('/residences', ResidenceController.list);
router.post('/residences', Auth.isAllowed([10]), ResidenceController.add);
router.put('/residences/:id', Auth.isAllowed([10]), ResidenceController.update);
router.delete('/residences/:id', Auth.isAllowed([10]), ResidenceController.remove);


/**
 * Sexualites
 */
router.get('/sexualites', SexualiteController.list);
router.post('/sexualites', Auth.isAllowed([10]), SexualiteController.add);
router.put('/sexualites/:id', Auth.isAllowed([10]), SexualiteController.update);
router.delete('/sexualites/:id', Auth.isAllowed([10]), SexualiteController.remove);


/**
 * Situations
 */
router.get('/situations', SituationController.list);
router.post('/situations', Auth.isAllowed([10]), SituationController.add);
router.put('/situations/:id', Auth.isAllowed([10]), SituationController.update);
router.delete('/situations/:id', Auth.isAllowed([10]), SituationController.remove);


/**
 * Alcools
 */
router.get('/alcools', AlcoolController.list);
router.post('/alcools', Auth.isAllowed([10]), AlcoolController.add);
router.put('/alcools/:id', Auth.isAllowed([10]), AlcoolController.update);
router.delete('/alcools/:id', Auth.isAllowed([10]), AlcoolController.remove);


/**
 * Tabacs
 */
router.get('/tabacs', TabacController.list);
router.post('/tabacs', Auth.isAllowed([10]), TabacController.add);
router.put('/tabacs/:id', Auth.isAllowed([10]), TabacController.update);
router.delete('/tabacs/:id', Auth.isAllowed([10]), TabacController.remove);


/**
 * Villes
 */
router.get('/villes', VilleController.list);


export default router;

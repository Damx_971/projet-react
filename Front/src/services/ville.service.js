import axios from 'axios';

export default class VilleService{

    static async list(){
        return await axios.get(`${process.env.REACT_APP_HOST_API}/villes`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenNetflix')}`
            }
        });
    }
}

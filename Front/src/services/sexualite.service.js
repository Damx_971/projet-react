import axios from 'axios';

export default class SexualiteService{

    static async list(){
        return await axios.get(`${process.env.REACT_APP_HOST_API}/sexualites`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenNetflix')}`
            }
        });
    }

    static async create(body){
        return await axios.post(`${process.env.REACT_APP_HOST_API}/sexualites`, body, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenNetflix')}`
            }
        });
    }

    static async update(id, body){
        return await axios.put(`${process.env.REACT_APP_HOST_API}/sexualites/${id}`, body, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenNetflix')}`
            }
        });
    }

    static async delete(id){
        return await axios.delete(`${process.env.REACT_APP_HOST_API}/sexualite/${id}`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenNetflix')}`
            }
        });
    }
}

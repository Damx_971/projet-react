import axios from 'axios';

export default class ResidenceService{

    static async list(){
        return await axios.get(`${process.env.REACT_APP_HOST_API}/residences`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenNetflix')}`
            }
        });
    }

    static async create(body){
        return await axios.post(`${process.env.REACT_APP_HOST_API}/residences`, body, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenNetflix')}`
            }
        });
    }

    static async update(id, body){
        return await axios.put(`${process.env.REACT_APP_HOST_API}/residences/${id}`, body, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenNetflix')}`
            }
        });
    }

    static async delete(id){
        return await axios.delete(`${process.env.REACT_APP_HOST_API}/residences/${id}`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('tokenNetflix')}`
            }
        });
    }
}

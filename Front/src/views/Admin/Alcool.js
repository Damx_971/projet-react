import React, {Component} from 'react';
import AlcoolService from "../../services/alcool.service";
import {Link} from 'react-router-dom';


export default class Alcool extends Component{

    constructor(props) {
        super(props);
        this.state = {
            alcools: [],
            alcool: null,
            updatedAlcool: {id: null, libelle: null}
        }
    }

    async componentDidMount() {
        try{
            let response = await AlcoolService.list();
            this.setState({alcools: response.data.alcools});
        }catch (e) {
            console.error(e);
        }
    }

    handleChange(e){
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    handleChangeUpdate(e){
        this.setState({
            updatedAlcool: {...this.state.updatedAlcool, libelle: e.target.value}
        });
    }

    async deleteAlcool(id){
        try{
            await AlcoolService.delete(id);
            let response = await AlcoolService.list();
            this.setState({alcools: response.data.alcools});
        }catch (e) {
            console.error(e);
        }
    }

    async submit(e){
        e.preventDefault();
        try{
            let body = {libelle: this.state.alcool};
            let response = await AlcoolService.create(body);
            let newAlcool = response.data.alcool;
            let {alcools} = this.state;
            alcools.push(newAlcool);
            this.setState({alcools, alcool: ''});
        }catch (e) {
            console.error(e);
        }
    }

    async submitUpdate(e){
        e.preventDefault();
        try {
            let body = {libelle: this.state.updatedAlcool.libelle};
            let response = await AlcoolService.update(this.state.updatedAlcool.id, body);
        }catch (e) {
            console.error(e);
        }
    }

    handleClickBtnModal(alcool){
        this.setState({updatedAlcool: {id: alcool._id, libelle: alcool.libelle}});
    }


    render() {
        let {alcools} = this.state;
        return <div className="container-fluid">

            {/*Navigation*/}
            <nav class="nav">
                <Link to={'/admin/alcool'} class="nav-link" href="#">Alcool</Link>
                <Link to={'/admin/tabac'}  class="nav-link" href="#">Tabac</Link>
                <Link to={'/admin/situation'} class="nav-link" href="#">Situation</Link>
                <Link to={'/admin/origine'} class="nav-link" href="#">Origine</Link>
                <Link to={'/admin/apparence'}  class="nav-link" href="#">Apparence</Link>
                <Link to={'/admin/residence'} class="nav-link" href="#">Résidence</Link>
                <Link to={'/admin/sexualite'} class="nav-link" href="#">Sexualité</Link>
            </nav>

            {/*Ajout*/}
            <form onSubmit={(e) => this.submit(e)}>
                <div className="form-group">
                    <label>Alcool</label>
                    <input type="text" id="alcool" className="form-control"
                        required
                            value={this.state.alcool}
                            onChange={(e) => this.handleChange(e)}/>
                </div>
                <button type="submit" className="btn btn-primary">Ajouter</button>
            </form>

            {/* Liste */}
            <table className="table">
                    <thead>
                        <tr>
                            <th>Libelle</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            alcools.map((alcool, index) => {
                                return <tr key={index}>
                                    <td>{alcool.libelle}</td>
                                    <td>
                                        <button type="button" className="btn btn-primary"
                                                data-toggle="modal"
                                                data-target="#modalUpdate"
                                            onClick={() => this.handleClickBtnModal(alcool)}>Modifier</button>

                                        <button className="btn btn-danger" onClick={() => this.deleteAlcool(alcool._id)}>
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-trash"
                                                 fill="#FFF" xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                                <path fillRule="evenodd"
                                                      d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                            </svg>
                                        </button>
                                    </td>
                                </tr>
                            })
                        }
                    </tbody>
                </table>
            
            {/* Modal update */}
            <div className="modal fade" id="modalUpdate" tabIndex="-1" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div className="modal-dialog">
                        <form onSubmit={(e) => this.submitUpdate(e)}>
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalLabel">Update alcool</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <div className="form-group">
                                        <label>Alcool</label>
                                        <input type="text" id="updatedAlcool" className="form-control"
                                               required
                                               value={this.state.updatedAlcool.libelle}
                                               onChange={(e) => this.handleChangeUpdate(e)}/>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="submit" className="btn btn-primary">Sauvegarder</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

        </div>
    }
}

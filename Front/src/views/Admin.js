import React, {Component} from 'react';
import {Link} from 'react-router-dom';


export default class Home extends Component{

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return <div className="container-fluid">

        {/*Navigation*/}
        <nav class="nav">
            <Link to={'/admin/alcool'} class="nav-link" href="#">Alcool</Link>
            <Link to={'/admin/tabac'}  class="nav-link" href="#">Tabac</Link>
            <Link to={'/admin/situation'} class="nav-link" href="#">Situation</Link>
            <Link to={'/admin/origine'} class="nav-link" href="#">Origine</Link>
            <Link to={'/admin/apparence'}  class="nav-link" href="#">Apparence</Link>
            <Link to={'/admin/residence'} class="nav-link" href="#">Résidence</Link>
            <Link to={'/admin/sexualite'} class="nav-link" href="#">Sexualité</Link>
        </nav>

        </div>
    }
}

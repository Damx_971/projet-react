import React, {Component} from 'react';
import DetailsCard from "../components/DetailsCard";
import UserService from "../services/user.service";

export default class Home extends Component{

    constructor(props) {
        super(props);
        this.state = {
            users: []
        }
    }

    async componentDidMount() {
        try{
            let response = await UserService.list();
            this.setState({users: response.data.users});
        }catch (e) {
            console.error(e);
        }
    }

    render() {
        let {users} = this.state;
        return <div className="container-fluid">

            <div className="row">
                {
                    users.map((user, index) => {
                        console.log(user);
                        return <div className="col-md-4 mb-3">
                            <DetailsCard
                                key={index}
                                nom={user.nom}
                                prenom={user.prenom}
                                sexe={user.sexe}
                                ville={user.ville}
                                thumbnail={`${process.env.REACT_APP_HOST_API}/${user.thumbnail}`}
                                aboutme={user.aboutme}
                                apparence={user.apparence}
                                taille={user.taille}
                                origine={user.origine}
                                enfants={user.enfants}
                                residence={user.residence}
                                sexualite={user.sexualite}
                                situation={user.situation}
                                alcool={user.alcool}
                                tabac={user.tabac}
                            />
                        </div>
                    })
                }
            </div>
        </div>
    }
}

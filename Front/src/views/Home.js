import React, {Component} from 'react';
import ProfilCard from "../components/ProfilCard";
import UserService from "../services/user.service";

export default class Home extends Component{

    constructor(props) {
        super(props);
        this.state = {
            users: []
        }
    }

    async componentDidMount() {
        try{
            let response = await UserService.list();
            this.setState({users: response.data.users});
        }catch (e) {
            console.error(e);
        }
    }

    render() {
        let {users} = this.state;
        return <div className="container-fluid">

            <div className="row">
                {
                    users.map((user, index) => {
                        console.log(user);
                        return <div className="col-md-4 mb-3">
                            <ProfilCard
                                key={index}
                                nom={user.nom}
                                prenom={user.prenom}
                                thumbnail={`${process.env.REACT_APP_HOST_API}/${user.thumbnail}`}
                                id={user._id}
                            />
                        </div>
                    })
                }
            </div>
        </div>
    }
}

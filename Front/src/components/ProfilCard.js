import React, {Component} from 'react';
import {Link} from 'react-router-dom';


export default class ProfilCard extends Component{

    render() {
        let {nom, prenom, thumbnail, id} = this.props;

        return <div className="card">
            {
                thumbnail && <img className="card-img-top" src={thumbnail} alt="Card image cap"/>
            }
            <div className="card-body">
                <h5 className="card-title">{nom}</h5>
                <h6 className="card-title">{prenom}</h6>
                <Link to={`/users/${id}`}  className="btn btn-primary">Voir le profil</Link>
            </div>
        </div>
    }
}


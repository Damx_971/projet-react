import React, {Component} from 'react';

export default class DetailsCard extends Component{

    render() {
        let {nom, prenom, thumbnail, sexe, ville, aboutme, apparence, taille, origine, enfants, residence, sexualite, situation, alcool, tabac} = this.props;

        return <div className="card">
            <div className="card-body">
                {
                    thumbnail && <img className="card-img-top" src={thumbnail} alt="Card image cap"/>
                }
                <h5 className="card-title">{nom} {prenom}</h5>
                <p className="card-title">Sexe : {sexe}</p>
                <p className="card-title">Ville : {ville}</p>
                <p className="card-title">A propos : {aboutme}</p>
                <p className="card-title">Apparence : {apparence}</p>
                <p className="card-title">Taille : {taille}</p>
                <p className="card-title">Origines : {origine}</p>
                <p className="card-title">Enfants : {enfants}</p>
                <p className="card-title">Résidence : {residence}</p>
                <p className="card-title">Sexualité : {sexualite}</p>
                <p className="card-title">Situation : {situation}</p>
                <p className="card-title">Alcool : {alcool}</p>
                <p className="card-title">Tabac : {tabac}</p>
            </div>
        </div>
    }
}

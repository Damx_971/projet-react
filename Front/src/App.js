import React, {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import './App.css';
import Home from "./views/Home";
import Header from "./components/Header";
import Login from "./views/Login";
import {connect} from 'react-redux';
import User_details from "./views/User_details";
import Admin from "./views/Admin";
import Alcool from "./views/Admin/Alcool";
import Tabac from "./views/Admin/Tabac";
import Origine from "./views/Admin/Origine";
import Residence from "./views/Admin/Residence";
import Apparence from "./views/Admin/Apparence";
import Sexualite from "./views/Admin/Sexualite";
import Situation from "./views/Admin/Situation";


class App extends Component{

    render() {
        return <BrowserRouter>
            <Header />

            {
                this.props.user && this.props.user.role === 10 && <div>
                    {/*ADMIN*/}
                    <Route exact path="/admin" component={Admin}/>
                    <Route exact path="/admin/alcool" component={Alcool}/>
                    <Route exact path="/admin/tabac" component={Tabac}/>
                    <Route exact path="/admin/situation" component={Situation}/>
                    <Route exact path="/admin/origine" component={Origine}/>
                    <Route exact path="/admin/sexualite" component={Sexualite}/>
                    <Route exact path="/admin/residence" component={Residence}/>
                    <Route exact path="/admin/apparence" component={Apparence}/>

                </div>
            }

            {/*HOME*/}
            <Route exact path="/" component={Home}/>

            {/*USER_DETAILS*/}
            <Route exact path="/users/:id" component={User_details}/>


            {/*LOGIN | REGISTER*/}
            <Route exact path="/login" component={Login}/>


        </BrowserRouter>
    }
}

const mapStateToProps = state => {
    return {user: state.user}
};

export default connect(mapStateToProps, null)(App);
